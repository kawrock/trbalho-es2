package atm.middleware.main;

import java.util.Date;

import atm.middleware.dao.*;
import atm.model.Administrador;
import atm.model.CartaoDeCredito;
import org.apache.log4j.Logger;

import atm.middleware.service.GerenciadorGeralImpl;
import atm.model.Cliente;

public class Main {

	private static final Logger logger = Logger.getLogger(Main.class);
	public static GerenciadorGeralImpl gerenciadorGeral;

	public static void main(String[] args) {

//		try {
			gerenciadorGeral = new GerenciadorGeralImpl();
//			ClienteDao clienteDao = new ClienteDaoImpl();
//			testaIncluiAlteraExcluiCliente(clienteDao);
//			CartaoDeCreditoDao cartaoDeCreditoDao = new CartaoDeCreditoDaoImpl();
//			testaIncluiExcluiCartaoDeCredito(cartaoDeCreditoDao);
//			//testaSaldoCartaoDeCredito() - select precisa ser atualizado
//			testaExtratoCartaoDeCredito();
			AdministradorDao administradorDao = new AdministradorDaoImpl();
		    testaIncluiAlteraExcluiAdministrador(administradorDao);

//			InterfaceRemotaImpl interfaceRemota = new InterfaceRemotaImpl();
//			interfaceRemota.setGerenciadorGeral(gerenciadorGeral);
//			InterfaceRemota stub = (InterfaceRemota) UnicastRemoteObject.exportObject(interfaceRemota, 0);
//	        Registry registry = LocateRegistry.getRegistry();
//	        registry.rebind("interface_remota", stub);
//	        logger.info("Sistema nucleo gerenciador do ATM pronto!");
//		} catch (RemoteException ex) {
//            logger.error("Remote exception.", ex);
//        }
	}

	private static void testaExtratoCartaoDeCredito() {
//		RF - O extrato de cartão de crédito deve mostrar ao Cliente as seguintes informações sobre
//		todas as transações efetuadas com cartões de crédito:
// 			Data da transação,
// 			hora da transação,
//			beneficiário da transação ou agente caso o beneficiário seja o próprio usuário
// 			tipo da transação
// 			valor da transação.
//		Extrato para 30, 60 e 90 dias.
	}

	private static void testaIncluiExcluiCartaoDeCredito(CartaoDeCreditoDao cartaoDeCreditoDao) {
		gerenciadorGeral.setCartaoDeCreditoDao(cartaoDeCreditoDao);
		CartaoDeCredito umCartaoDeCredito = new CartaoDeCredito();
		umCartaoDeCredito.setLimite(1000);
		umCartaoDeCredito.setNumero("11110000");
		umCartaoDeCredito.setDataDeValidade(new Date());
		umCartaoDeCredito.setNomeCartao("NOME DO TITULAR");
		umCartaoDeCredito.setBandeira("VISA");
		umCartaoDeCredito.setCodigoDeSeguranca("123");
		gerenciadorGeral.incluiCartaoDeCredito(umCartaoDeCredito);
		gerenciadorGeral.excluiCartaoDeCredito(umCartaoDeCredito);
	}

	private static void testaIncluiAlteraExcluiCliente(ClienteDao clienteDao) {
		gerenciadorGeral.setClienteDao(clienteDao);
		Cliente umCliente = new Cliente();
		umCliente.setCpf("55555555555");
		umCliente.setEndereco("Alguma rua, algum bairro, alguma cidade");
		umCliente.setDataDeNascimento(new Date());
		umCliente.setNomeDaMae("Maria");
		umCliente.setNomeDoPai("Joao");
		umCliente.setRg("1234132");
		umCliente.setNome("Joaozinho");
		gerenciadorGeral.incluiCliente(umCliente);
		gerenciadorGeral.alteraCliente(umCliente);
		gerenciadorGeral.excluiCliente(umCliente);
	}

	private static void testaIncluiAlteraExcluiAdministrador(AdministradorDao administradorDao) {
		gerenciadorGeral.setAdministradorDao(administradorDao);
		Administrador umAdministrador = new Administrador();
		umAdministrador.setCpf("44444444444");
		umAdministrador.setNome("Pedrinho");
		umAdministrador.setCodigoIdentificador("2020202020");
		umAdministrador.setCargo("Subgerente");

		gerenciadorGeral.incluiAdministrador(umAdministrador);
		//System.out.println(gerenciadorGeral.buscarAdministrador(3).getNome());
		System.out.println(gerenciadorGeral.buscarAdministrador("Pedrinho").get(0).getId());
		gerenciadorGeral.alteraAdministrador(umAdministrador);
		gerenciadorGeral.excluiAdministrador(umAdministrador);


	}
}
