package atm.middleware.main;


import org.apache.log4j.Logger;
import java.util.Date;

import atm.middleware.dao.ClienteDao;
import atm.middleware.dao.ClienteDaoImpl;
import atm.middleware.dao.ContaDao;
import atm.middleware.dao.ContaDaoImpl;
import atm.middleware.dao.MovimentacaoDao;
import atm.middleware.dao.MovimentacaoDaoImpl;
import atm.middleware.dao.PagamentoDao;
import atm.middleware.dao.PagamentoDaoImpl;
import atm.middleware.lipermi.FrontendServiceImpl;
import atm.middleware.service.GerenciadorGeralImpl;
import atm.model.Cliente;
import atm.model.Conta;

public class MainForAtm {

	private static final Logger logger = Logger.getLogger(MainForAtm.class);

	public static void main(String[] args) {
		logger.info("Iniciando programa Middleware para o sistema frontend ATM");
		// Inicializando o gerenciador geral
		GerenciadorGeralImpl gerenciadorGeral = new GerenciadorGeralImpl();
		ClienteDao clienteDao = new ClienteDaoImpl();
		ContaDao contaDao = new ContaDaoImpl();
		MovimentacaoDao movimentacaoDao = new MovimentacaoDaoImpl();
		PagamentoDao pagamentoDao = new PagamentoDaoImpl();
		gerenciadorGeral.setClienteDao(clienteDao);
		gerenciadorGeral.setContaDao(contaDao);
		gerenciadorGeral.setMovimentacaoDao(movimentacaoDao);
		gerenciadorGeral.setPagamentoDao(pagamentoDao);
		
		// Inicializando o LipeRMI
		FrontendServiceImpl frontendService = new FrontendServiceImpl();
		frontendService.setGerenciadorGeral(gerenciadorGeral);
		logger.info("Programa Middleware para o sistema frontend ATM pronto!");
		
		Cliente umCliente = new Cliente();
		umCliente.setCpf("55555555555");
		umCliente.setEndereco("Alguma rua, algum bairro, alguma cidade");
		umCliente.setDataDeNascimento(new Date());
		umCliente.setNomeDaMae("Julia");
		umCliente.setNomeDoPai("Joao");
		umCliente.setRg("1234132");
		umCliente.setNome("Joaozinho");
		gerenciadorGeral.incluiCliente(umCliente);
		//gerenciadorGeral.alteraCliente(umCliente);
		//gerenciadorGeral.excluiCliente(umCliente);
//		
//		Conta conta = gerenciadorGeral.buscaContaPeloCartao("123");
//		long saldo = gerenciadorGeral.saldoConta(conta);
//		logger.debug("Saldo: " + saldo);
	
	}
}
