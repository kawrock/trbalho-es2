package atm.middleware.main;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.apache.log4j.Logger;

import atm.middleware.dao.ClienteDao;
import atm.middleware.dao.ClienteDaoImpl;
import atm.middleware.dao.ContaDao;
import atm.middleware.dao.ContaDaoImpl;
import atm.middleware.dao.MovimentacaoDao;
import atm.middleware.dao.MovimentacaoDaoImpl;
import atm.middleware.dao.PagamentoDao;
import atm.middleware.dao.PagamentoDaoImpl;
import atm.middleware.rmi.InterfaceRemotaImpl;
import atm.middleware.service.GerenciadorGeralImpl;
import atm.rmi.InterfaceRemota;

public class MainForGerenciador {

	private static final Logger logger = Logger.getLogger(MainForGerenciador.class);

	public static void main(String[] args) {
		try {
			logger.info("Iniciando programa Middleware para o sistema Gerenciador");
			// Inicializando o gerenciador geral
			GerenciadorGeralImpl gerenciadorGeral = new GerenciadorGeralImpl();
			ClienteDao clienteDao = new ClienteDaoImpl();
			ContaDao contaDao = new ContaDaoImpl();
			MovimentacaoDao movimentacaoDao = new MovimentacaoDaoImpl();
			PagamentoDao pagamentoDao = new PagamentoDaoImpl();
			gerenciadorGeral.setClienteDao(clienteDao);
			gerenciadorGeral.setContaDao(contaDao);
			gerenciadorGeral.setMovimentacaoDao(movimentacaoDao);
			gerenciadorGeral.setPagamentoDao(pagamentoDao);

			// Inicializando o RMI
			InterfaceRemotaImpl interfaceRemota = new InterfaceRemotaImpl();
			interfaceRemota.setGerenciadorGeral(gerenciadorGeral);
			InterfaceRemota stub = (InterfaceRemota) UnicastRemoteObject.exportObject(interfaceRemota, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("interface_remota", stub);
			logger.info("Programa Middleware para o sistema Gerenciador pronto!");
		} catch (RemoteException ex) {
			logger.error("Remote exception.", ex);
		}
	}
}
