package atm.middleware.rmi;

import java.rmi.RemoteException;
import java.util.List;
import atm.model.Administrador;
import org.apache.log4j.Logger;

import atm.middleware.service.GerenciadorGeral;
import atm.model.CartaoDeCredito;
import atm.model.Cliente;
import atm.model.Conta;
import atm.rmi.InterfaceRemota;
import atm.model.CartaoDeDebito;

import java.rmi.RemoteException;

public class InterfaceRemotaImpl implements InterfaceRemota {

	// Properties
	private static final Logger logger = Logger.getLogger(InterfaceRemotaImpl.class);
	
	private GerenciadorGeral gerenciadorGeral;
	
	// Methods
	
	@Override
	public boolean incluiCliente(Cliente umCliente) {
		logger.debug("Incluindo cliente");
		boolean result = gerenciadorGeral.incluiCliente(umCliente);
		logger.debug("Cliente incluido");
		return result;
	}

	@Override
	public boolean alteraCliente(Cliente umCliente) {
		logger.debug("Alterando cliente");
		boolean result = gerenciadorGeral.alteraCliente(umCliente);
		logger.debug("Cliente alterado");
		return result;
	}

	@Override
	public boolean excluiCliente(Cliente umCliente) {
		logger.debug("Excluindo cliente");
		boolean result = gerenciadorGeral.excluiCliente(umCliente);
		logger.debug("Cliente excluído");
		return result;
	}

	@Override
	public boolean incluiCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) throws RemoteException {
		logger.debug("Incluindo cartao de credito");
		boolean result = gerenciadorGeral.incluiCartaoDeCredito(umCartaoDeCredito);
		logger.debug("Cartao de credito incluido");
		return result;
	}

	@Override
	public boolean excluiCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) throws RemoteException {
		logger.debug("Excluindo cartao de credito");
		boolean result = gerenciadorGeral.excluiCartaoDeCredito(umCartaoDeCredito);
		logger.debug("Cartao de credito excluído");
		return result;
	}

	@Override
	public long saldoCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) {
		return gerenciadorGeral.saldoCartaoDeCredito(umCartaoDeCredito);
	}
        @Override
	public boolean incluiCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) throws RemoteException {
		logger.debug("Incluindo cartão de débito");
		boolean result = gerenciadorGeral.incluiCartaoDeDebito(umCartaoDeDebito);
		logger.debug("Cartão de débito incluído");
		return result;
	}

	@Override
	public boolean excluiCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) throws RemoteException {
		logger.debug("Excluindo cartão de débito");
		boolean result = gerenciadorGeral.excluiCartaoDeDebito(umCartaoDeDebito);
		logger.debug("Cartão de débito excluído");
		return result;
	}
	@Override
	public long saldoCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) {
		return gerenciadorGeral.saldoCartaoDeDebito(umCartaoDeDebito);
	}
	@Override
	public boolean incluiConta(Conta conta) throws RemoteException {
		return gerenciadorGeral.incluiConta(conta);
	}
	
	@Override
	public boolean excluiConta(Conta conta) throws RemoteException {
		return gerenciadorGeral.incluiConta(conta);
	}
	
	@Override
	public boolean alteraConta(Conta conta) throws RemoteException {
		return gerenciadorGeral.incluiConta(conta);
	}
	
	@Override
	public Conta buscarConta(String numero) throws RemoteException {
		return gerenciadorGeral.buscarConta(numero);
	}
	
	@Override
	public Conta buscarContaPeloCartao(String numeroCartao) throws RemoteException{
		return gerenciadorGeral.buscaContaPeloCartao(numeroCartao);
	}
	
	@Override
	public Cliente buscarCliente(long id) throws RemoteException {
		return gerenciadorGeral.buscarCliente(id);
	}
	
	@Override
	public List<Cliente> buscarClientes(String nome) throws RemoteException {
		return gerenciadorGeral.buscarCliente(nome);
	}
	
	
	// Set

	@Override
	public boolean incluiAdministrador(Administrador umAdministrador) {
		logger.debug("Incluindo administrador");
		boolean result = gerenciadorGeral.incluiAdministrador(umAdministrador);
		logger.debug("Administrador incluido");
		return result;
	}

	@Override
	public boolean alteraAdministrador(Administrador umAdministrador) {
		logger.debug("Alterando administrador");
		boolean result = gerenciadorGeral.alteraAdministrador(umAdministrador);
		logger.debug("Administrador alterado");
		return result;
	}

	@Override
	public boolean excluiAdministrador(Administrador umAdministrador) {
		logger.debug("Excluindo administrador");
		boolean result = gerenciadorGeral.excluiAdministrador(umAdministrador);
		logger.debug("Administrador excluído");
		return result;
	}

	public void setGerenciadorGeral(GerenciadorGeral gg) {
		this.gerenciadorGeral = gg;
	}
}
