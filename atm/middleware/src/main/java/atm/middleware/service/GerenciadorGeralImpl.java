package atm.middleware.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import atm.middleware.dao.AdministradorDao;
import atm.middleware.dao.CartaoDeCreditoDao;
import atm.middleware.dao.CartaoDeDebitoDao;
import atm.middleware.dao.ClienteDao;
import atm.middleware.dao.ContaDao;
import atm.middleware.dao.DepositoDao;
import atm.middleware.dao.MovimentacaoDao;
import atm.middleware.dao.PagamentoDao;
import atm.middleware.util.Conexao;
import atm.model.Administrador;
import atm.model.CartaoDeCredito;
import atm.model.Cliente;
import atm.model.Conta;
import atm.model.Deposito;
import atm.model.Movimentacao;
import atm.model.Pagamento;
import atm.model.Transferencia;
import atm.model.Deposito;
import atm.model.CartaoDeDebito;

public class GerenciadorGeralImpl implements GerenciadorGeral {

	// Properties
	
	private static final Logger logger = Logger.getLogger(GerenciadorGeralImpl.class);
	
	private ClienteDao clienteDao;
	
	private CartaoDeCreditoDao cartaoDeCreditoDao;
	
	private AdministradorDao administradorDao;
	
	private ContaDao contaDao;

	private MovimentacaoDao movimentacaoDao;

	private PagamentoDao pagamentoDao;
        
        private DepositoDao depositoDao;
        
        private CartaoDeDebitoDao cartaoDeDebitoDao;
	
	// Constructor
	
	public GerenciadorGeralImpl() {}
	
	// Methods
	
	@Override
	public boolean incluiCliente(Cliente umCliente) {
		logger.info("Incluindo cliente: " + umCliente);
		Conexao.beginTransaction();
		boolean result = clienteDao.inclui(umCliente);
		if (result)
			Conexao.commitTransaction();
		else {
			Conexao.rollbackTransaction();
			throw new RuntimeException("Erro na inclusão do cliente");
		}
		return result;
	}

	@Override
	public boolean alteraCliente(Cliente umCliente) {
		Conexao.beginTransaction();
		boolean result = clienteDao.altera(umCliente);
		if (result)
			Conexao.commitTransaction();
		else {
			Conexao.rollbackTransaction();
			throw new RuntimeException("Erro na alteração do cliente");
		}
		return result;
	}

	@Override
	public boolean excluiCliente(Cliente umCliente) {
		
		Conexao.beginTransaction();
		boolean result = clienteDao.exclui(umCliente);
		if (result)
			Conexao.commitTransaction();
		else {
			Conexao.rollbackTransaction();
			throw new RuntimeException("Erro na exclusão do cliente");
		}
		return result;
	}
	
	@Override
	public boolean incluiConta(Conta conta) {
		Conexao.beginTransaction();
		boolean result = contaDao.inclui(conta);
		if (result)
			Conexao.commitTransaction();
		else {
			Conexao.rollbackTransaction();
			throw new RuntimeException("Erro na inclusão de conta");
		}
		return result;
	}

	@Override
	public boolean excluiConta(Conta conta) {
		
		if(conta.getSaldo() == 0)
		{
			Conexao.beginTransaction();
			boolean result = contaDao.exclui(conta);
			if (result)
				Conexao.commitTransaction();
			else {
				Conexao.rollbackTransaction();
				throw new RuntimeException("Erro na exclusão de conta");
			}
			return result;
		}
		else
		{
			logger.info("Conta nao pode ser excluida, verifique o saldo");
			throw new RuntimeException("Conta nao pode ser excluida, verifique o saldo");
		}
	}
	
	@Override
	public boolean alteraConta(Conta conta) {
		
		Conexao.beginTransaction();
		boolean result = contaDao.altera(conta);
		if (result)
			Conexao.commitTransaction();
		else {
			Conexao.rollbackTransaction();
			throw new RuntimeException("Erro na alteração da conta");
		}
		return result;
	}
	
	@Override
	public Conta buscaContaPeloCartao (String numeroCartao){
		
		logger.debug("buscando conta pelo cartão: " + numeroCartao);
		Conta result = null;
		try {
			result = contaDao.buscaContaPeloCartao(numeroCartao);
		}finally {
			Conexao.closeConnection();
		}
		if(result != null){
			logger.debug("conta buscada: " + result);
		} else {
			logger.debug("Conta nula");
		}
		
		return result;
	}
	
	public long saldoConta (Conta conta){
		conta = recuperaUmaContaCompleta(conta);
		return conta.getSaldo();
	}
	
	public List<Movimentacao> extratoConta(Conta conta, int dias){
		conta = recuperaUmaContaCompleta(conta);
		
		List<Movimentacao> movimentacoes = conta.getExtrato(dias);
		Collections.sort(movimentacoes, new Comparator<Movimentacao>(){

			@Override
			public int compare(Movimentacao arg0, Movimentacao arg1) {
				if (arg0.getData().before(arg1.getData())){
					return -1;
				}
				else if (arg0.getData().equals(arg1.getData())){
					return 0;
				}
				else{
					return 1;
				}
			}
		});
		return movimentacoes;
	}

	@Override
	public Cliente buscarCliente(long id) 
	{
		return clienteDao.buscarCliente(id);
	}
	
	@Override
	public Conta buscarConta(String numero) 
	{
		return contaDao.buscarConta(numero);
	}

	@Override
	public List<Cliente> buscarCliente(String nome) 
	{
		return clienteDao.buscarCliente(nome);
	}

	public boolean incluiCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) {
		Conexao.beginTransaction();
		boolean result = cartaoDeCreditoDao.inclui(umCartaoDeCredito);
		if (result)
			Conexao.commitTransaction();
		else
			Conexao.rollbackTransaction();
		return result;
	}


	@Override
	public boolean excluiCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) {
		logger.debug("excluindo o cartão de crédito: " + umCartaoDeCredito);
		boolean result = false;
		try {
			result = cartaoDeCreditoDao.exclui(umCartaoDeCredito);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("cartão de crédito excluído");
		return result;
	}

	@Override
	public long saldoCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) {
		logger.debug("obtendo saldo do cartão de crédito: " + umCartaoDeCredito);
		long result = 0L;
		try {
			result = cartaoDeCreditoDao.saldo(umCartaoDeCredito);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("saldo do cartão de crédito obtido: " + result);
		return result;
	}

	@Override
	public boolean incluiMovimentacao(Movimentacao umaMovimentacao) {
		logger.debug("incluindo a movimentação: " + umaMovimentacao);
		boolean result = false;
		try {
			result = movimentacaoDao.inclui(umaMovimentacao);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("id da movimentação incluida: " + umaMovimentacao.getId());
		return result;
	}

	public boolean incluiAdministrador(Administrador umAdministrador) {
		logger.info("Incluindo administrador: " + umAdministrador);
		Conexao.beginTransaction();

		boolean result = administradorDao.inclui(umAdministrador);
		if (result)
			Conexao.commitTransaction();
		else {
			Conexao.rollbackTransaction();
			throw new RuntimeException("Erro na inclusão do administrador");
		}
		return result;
	}

	@Override
	public List<Movimentacao> bucarMovimentacaoPorConta(Conta umaConta) {
		logger.debug("buscando movimentações da conta: " + umaConta);
		List<Movimentacao> result = null;
		try {
			result = movimentacaoDao.bucarMovimentacaoPorConta(umaConta);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("lista de movimentações buscada: " + result);
		return result;
	}
        
        @Override
	public boolean incluiDeposito(Deposito umDeposito) {
		logger.debug("incluindo o deposito: " + umDeposito);
		boolean result = false;
		try {
			result = depositoDao.inclui(umDeposito);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("deposito incluido");
		return result;
	}
	
	@Override
	public boolean alteraAdministrador(Administrador umAdministrador) {
		Conexao.beginTransaction();
		boolean result = administradorDao.altera(umAdministrador);
		if (result)
			Conexao.commitTransaction();
		else {
			Conexao.rollbackTransaction();
			throw new RuntimeException("Erro na alteração do administrador");
		}
		return result;
	}

	@Override
	public boolean excluiAdministrador(Administrador umAdministrador) {
		Conexao.beginTransaction();
		boolean result = administradorDao.exclui(umAdministrador);
		if (result)
			Conexao.commitTransaction();
		else {
			Conexao.rollbackTransaction();
			throw new RuntimeException("Erro na exclusão do administrador");
		}
		return result;
	}

	@Override
	public Administrador buscarAdministrador(long id) {
		logger.debug("buscando administrador por ID: " + id);
		Administrador result = null;
		try {
			result = administradorDao.buscarAdministrador(id);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("administrador buscado: " + result);
		return result;
	}

	public List<Administrador> buscarAdministrador(String nome) {
		logger.debug("buscando administrador por nome: " + nome);
		List<Administrador> result = null;
		try {
			result = administradorDao.buscarAdministrador(nome);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("administradores buscados: " + result);
		return result;
	}

	@Override
	public boolean efetuaUmPagamento(Pagamento umPagamento) {
		logger.debug("efetuando o pagamento: " + umPagamento);
		boolean result = false;
		try {
			result = pagamentoDao.inclui(umPagamento);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("ID do pagamento efetuado: " + umPagamento.getId());
		return result;
	}

	@Override
	public boolean efetuaUmaTransferencia(Transferencia umaTransferencia) {
		logger.debug("efetuando a transferencia: " + umaTransferencia);
		boolean result = false;
		try {
			Conexao.beginTransaction();
			// Verificar que as contas existem
			Conta contaDeOrigem = contaDao.buscarConta(umaTransferencia.getContaDeOrigem().getNumero());
			Conta contaDeDestino = contaDao.buscarConta(umaTransferencia.getContaDeDestino().getNumero());
			Movimentacao umaMovimentacao = new Movimentacao();
			umaMovimentacao.setConta_a(contaDeOrigem);
			umaMovimentacao.setCliente(contaDeOrigem.getProprietario());
			umaMovimentacao.setConta_b(contaDeDestino);
			umaMovimentacao.setData(new Date());
			umaMovimentacao.setTipo(Movimentacao.TRANSFERENCIA);
			umaMovimentacao.setEntidade("<Precisamos definir o que vai aqui!!!>");
			umaMovimentacao.setValor(umaTransferencia.getValor()*-1);
                        umaMovimentacao.setHistorico("Transf." + contaDeOrigem.getNumero() + "para" + contaDeDestino.getNumero());
			movimentacaoDao.inclui(umaMovimentacao);
			umaMovimentacao.setCliente(contaDeDestino.getProprietario());
                        umaMovimentacao.setConta_a(contaDeDestino);
			umaMovimentacao.setConta_b(null);
			umaMovimentacao.setValor(umaTransferencia.getValor());
                        umaMovimentacao.setHistorico("Transf." + contaDeOrigem.getNumero() + "para" + contaDeDestino.getNumero());
			movimentacaoDao.inclui(umaMovimentacao);
			Conexao.commitTransaction();
			result = true;
		} finally {
			Conexao.rollbackTransaction();
			Conexao.closeConnection();
		}
		logger.debug("transferencia efetuada");
		return result;
	}
        
        @Override
	public boolean efetuaUmDeposito(Deposito umDeposito) {
		logger.debug("efetuando a transferencia: " + umDeposito);
		boolean result = false;
		try {
			Conexao.beginTransaction();
			// Verificar que as contas existem
			Conta contaDeOrigem = contaDao.buscarConta(umDeposito.getContaDeOrigem().getNumero());
			Conta contaDeDestino = contaDao.buscarConta(umDeposito.getContaDeDestino().getNumero());
			umDeposito.setContaDeOrigem(contaDeOrigem);
			umDeposito.setContaDeDestino(contaDeDestino);
			umDeposito.setCheque("222333444");
                        umDeposito.setValor(umDeposito.getValor()*-1);
			umDeposito.setData(new Date());
			depositoDao.inclui(umDeposito);
			Conexao.commitTransaction();
			result = true;
		} finally {
			Conexao.rollbackTransaction();
			Conexao.closeConnection();
		}
		logger.debug("deposito efetuado");
		return result;
	}

	public Conta recuperaUmaContaCompleta(Conta umaConta) {
		Conta result = null;
		logger.debug("recuperando uma conta e todos os objetos relacionados: " + umaConta);
		try {
			result = contaDao.buscarConta(umaConta.getNumero());
			result.setProprietario(clienteDao.buscarCliente(result.getProprietario().getId()));
			result.setMovimentacoes(movimentacaoDao.bucarMovimentacaoPorConta(result));
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("conta recuperada: " + result);
		return result;
	}
        
        @Override
	public boolean incluiCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) {
		Conexao.beginTransaction();
		boolean result = cartaoDeDebitoDao.inclui(umCartaoDeDebito);
		if (result)
			Conexao.commitTransaction();
		else
			Conexao.rollbackTransaction();
		return result;
	}


	@Override
	public boolean excluiCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) {
		logger.debug("Excluindo o cartão de débito: " + umCartaoDeDebito);
		boolean result = false;
		try {
			result = cartaoDeDebitoDao.exclui(umCartaoDeDebito);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("Cartão de débito excluído");
		return result;
	}

	@Override
	public long saldoCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) {
		logger.debug("Obtendo saldo do cartão de débito: " + umCartaoDeDebito);
		long result = 0L;
		try {
			result = cartaoDeDebitoDao.saldo(umCartaoDeDebito);
		} finally {
			Conexao.closeConnection();
		}
		logger.debug("Saldo do cartão de débito obtido: " + result);
		return result;
	}

	
	// Set
	
	public void setClienteDao(ClienteDao clienteDao) {
		
		this.clienteDao = clienteDao;
	}

	public void setContaDao(ContaDao contaDao) {
		
		this.contaDao = contaDao;
	}

	public void setCartaoDeCreditoDao(CartaoDeCreditoDao cartaoDeCreditoDao) {
		
		this.cartaoDeCreditoDao = cartaoDeCreditoDao;
	}

	public void setAdministradorDao(AdministradorDao administradorDao) {

		this.administradorDao = administradorDao;
	}

	public void setMovimentacaoDao(MovimentacaoDao movimentacaoDao) {

		this.movimentacaoDao = movimentacaoDao;
	}

	public void setPagamentoDao(PagamentoDao pagamentoDao) {

		this.pagamentoDao = pagamentoDao;
	}
        
        public void setDepositoDao(DepositoDao depositoDao) {

		this.depositoDao = depositoDao;
	} 
        public void setCartaoDeDebitoDao(CartaoDeDebitoDao cartaoDeDebitoDao) {

		this.cartaoDeDebitoDao = cartaoDeDebitoDao;
	}}