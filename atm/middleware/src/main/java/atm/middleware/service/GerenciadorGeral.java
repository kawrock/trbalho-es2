package atm.middleware.service;

import java.util.List;

import atm.model.Administrador;
import atm.model.CartaoDeCredito;

import atm.model.Cliente;
import atm.model.Conta;
import atm.model.Movimentacao;
import atm.model.Pagamento;
import atm.model.Transferencia;
import atm.model.Deposito;
import atm.model.CartaoDeDebito;

public interface GerenciadorGeral {
	
	boolean incluiCliente(Cliente umCliente);
	
	boolean alteraCliente(Cliente umCliente);
	
	boolean excluiCliente(Cliente umCliente);

	boolean incluiConta(Conta conta);

	boolean excluiConta(Conta conta);

	boolean alteraConta(Conta conta);

	Cliente buscarCliente(long id);

	Conta buscarConta(String numero);
	
	Conta buscaContaPeloCartao (String numeroCartao);
	
	long saldoConta (Conta conta);
	
	List<Movimentacao> extratoConta(Conta conta, int dias);
	
	List<Cliente> buscarCliente(String nome);

	boolean incluiCartaoDeCredito(CartaoDeCredito umCartaoDeCredito);

	boolean excluiCartaoDeCredito(CartaoDeCredito umCartaoDeCredito);

	long saldoCartaoDeCredito(CartaoDeCredito umCartaoDeCredito);
	
	boolean incluiMovimentacao(Movimentacao umaMovimentacao);
        
    boolean incluiDeposito(Deposito umDeposito);
	
	List<Movimentacao> bucarMovimentacaoPorConta(Conta umaConta);

	boolean incluiAdministrador(Administrador umAdministrador);

	boolean alteraAdministrador(Administrador umAdministrador);

	boolean excluiAdministrador(Administrador umAdministrador);

	Administrador buscarAdministrador(long id);

	List<Administrador> buscarAdministrador(String nome);

	boolean efetuaUmPagamento(Pagamento umPagamento);
	
	boolean efetuaUmaTransferencia(Transferencia umaTransferencia);
	
	Conta recuperaUmaContaCompleta(Conta umaConta);
        
    boolean efetuaUmDeposito(Deposito umDeposito);

    boolean incluiCartaoDeDebito(CartaoDeDebito umCartaoDeDebito);

	boolean excluiCartaoDeDebito(CartaoDeDebito umCartaoDeDebito);

	long saldoCartaoDeDebito(CartaoDeDebito umCartaoDeDebito);
}
