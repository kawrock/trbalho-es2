package atm.middleware.lipermi;

import java.io.IOException;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.List;

import org.apache.log4j.Logger;

import atm.lipermi.FrontendService;
import atm.middleware.service.GerenciadorGeral;
import atm.model.Conta;
import atm.model.Movimentacao;
import atm.model.Pagamento;
import atm.model.Transferencia;
import atm.model.Deposito;
import net.sf.lipermi.exception.LipeRMIException;
import net.sf.lipermi.handler.CallHandler;
import net.sf.lipermi.net.IServerListener;
import net.sf.lipermi.net.Server;

public class FrontendServiceImpl implements FrontendService {

	// Properties
	
	private static final Logger logger = Logger.getLogger(FrontendServiceImpl.class);
	
	private GerenciadorGeral gerenciadorGeral;
	
	// Constructors
	
	public FrontendServiceImpl() {
		try {
			CallHandler callHandler = new CallHandler();
			callHandler.registerGlobal(FrontendService.class, this);
			Server server = new Server();
			server.bind(7777, callHandler);
			server.addServerListener(new IServerListener() {

				@Override
				public void clientDisconnected(Socket socket) {
					System.out.println("Client Disconnected: " + socket.getInetAddress());
				}

				@Override
				public void clientConnected(Socket socket) {
					System.out.println("Client Connected: " + socket.getInetAddress());
				}
			});
			System.out.println("Server Listening");
		} catch (LipeRMIException e) {
			logger.error("Erro da API LipeRMI:", e);
		} catch (IOException e) {
			logger.error("Erro de IO junto ao Service do ATM:", e);
		}
	}
	
	// Methods

	@Override
	public Conta buscarContaPeloCartao(String numeroCartao){
		return gerenciadorGeral.buscaContaPeloCartao(numeroCartao);
	} 
	
	@Override
	public long saldoConta (Conta conta){
		return gerenciadorGeral.saldoConta(conta);
	}
	
	@Override
	public List<Movimentacao> extratoConta (Conta conta, int dias){
		return gerenciadorGeral.extratoConta(conta, dias);
	}

	@Override
	public boolean efetuaUmPagamento(Pagamento umPagamento) {
		logger.debug("efetuando um pagamento");
		boolean result = gerenciadorGeral.efetuaUmPagamento(umPagamento);
		logger.debug("um pagamento efetuado");
		return result;
	}

	@Override
	public boolean efetuaUmaTransferencia(Transferencia umaTransferencia) {
		logger.debug("efetuando uma transferencia");
//		boolean result = gerenciadorGeral.pagamento(umPagamento);
		logger.debug("uma transferencia efetuada");
//		return result;
		return true;
	}
        
    @Override
	public boolean efetuaUmDeposito(Deposito umDeposito) {
		logger.debug("efetuando um Deposito");
//		boolean result = gerenciadorGeral.pagamento(umPagamento);
		logger.debug("um Deposito efetuado");
//		return result;
		return true;
	}
	
	// Set
	
	public void setGerenciadorGeral(GerenciadorGeral gg) {
		this.gerenciadorGeral = gg;
	}

}
