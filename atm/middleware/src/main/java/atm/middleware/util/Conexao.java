package atm.middleware.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class Conexao {

	// Properties
	
	private static final Logger logger = Logger.getLogger(Conexao.class);
	
	private final static String usuario = "root";
	
	private final static String senha = "Dia11###";
//	private final static String senha = "abcd1234";
	
//	private static Conexao conexao = null;
	
//	private static Connection conn;
	
	private static final ThreadLocal<Connection> threadConnection = new ThreadLocal<Connection>();
	
	// Constructors
	
	private Conexao() {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			logger.debug("Classe não encontrada");
		}
	}
	
	// Methods
	
	public static void beginTransaction() {
		
		Connection con = (Connection) threadConnection.get();
		try {
			if (con == null) {
				con = DriverManager.getConnection("jdbc:mysql://localhost/desenv?autoReconnect=true&useSSL=false", 
						usuario, senha);
			}
			con.setAutoCommit(false);
			threadConnection.set(con);
		} catch (SQLException e) {
			logger.error("", e);
		}
	}
	
	public static void commitTransaction() {
		
		Connection con = (Connection) threadConnection.get();
		try {
			if (con != null) {
//            System.err.print("Transaction is being rolled back");
				con.commit();
			}
        } catch(SQLException e) {
        	logger.error("", e);
        } finally {
        	try { if (con != null) con.close(); } catch(Exception e) { };
        	threadConnection.set(null);
        }
	}

	public static void rollbackTransaction() {
		
		Connection con = (Connection) threadConnection.get();
		try {
			if (con != null) {
//            System.err.print("Transaction is being rolled back");
				con.rollback();
			}
        } catch(SQLException e) {
        	logger.error("", e);
        } finally {
        	try { if (con != null) con.close(); } catch(Exception e) { };
        	threadConnection.set(null);
        }
	}
	
	public static Connection getConnection() {
		
		Connection con = (Connection) threadConnection.get();
		try {
			if (con == null) {
				con = DriverManager.getConnection("jdbc:mysql://localhost/desenv?autoReconnect=true&useSSL=false", 
						usuario, senha);
				threadConnection.set(con);
			}
		} catch (SQLException e) {
			logger.error("", e);
		}
		return con;
	}
	
	public static void closeConnection() {
		
		Connection con = (Connection) threadConnection.get();
		try { 
			if (con != null) 
				con.close(); 
		} catch(Exception e) {
			logger.error("", e);
		}
		threadConnection.set(null);
	}
}
