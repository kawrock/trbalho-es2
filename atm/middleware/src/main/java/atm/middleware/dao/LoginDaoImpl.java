package atm.middleware.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import atm.middleware.util.Conexao;
import atm.model.Login;

public class LoginDaoImpl implements LoginDao {
	// Properties
	private static final Logger logger = Logger.getLogger(LoginDaoImpl.class);

	private static final String INSERT_TB_LOGIN_SQL = 
			"insert into tb_login(login,senha)"
			+"values (?,?)";
	
	private static final String SELECT_TB_LOGIN_LOGIN_SQL = 
			"select login from tb_login" 
			+"where login=?";
	
	private static final String SELECT_TB_LOGIN_SENHA_SQL = 
			"select senha from tb_login" 
			+"where login=?";

	@Override
	public boolean validaUsuario(Login login) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet result = null;	
		
		try
		{
			logger.debug("buscando informacoes do login do usuario");
			connection = Conexao.getConnection();
			pstmt = connection.prepareStatement(SELECT_TB_LOGIN_LOGIN_SQL);
			pstmt.setString(1, login.getUsuario());
			
			result = pstmt.executeQuery();
			
			if (result.next()){
				logger.debug("informações encontradas do login do usuario: " + login.getUsuario());
				return true;
			}
			pstmt.close();
			result.close();
			
		} catch (SQLException e) {
			logger.error("Erro ao procurar Login do usuario: " + login.getUsuario(), e);
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return false;
}

	@Override
	public boolean validaSenha(Login login) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet result = null;	
		
		try
		{
			logger.debug("buscando informacoes da senha do usuario");
			connection = Conexao.getConnection();
			pstmt = connection.prepareStatement(SELECT_TB_LOGIN_SENHA_SQL);
			pstmt.setString(1, login.getUsuario());
			
			result = pstmt.executeQuery();
			
			if (result.next() && result.getString(1)==login.getSenha()){
				logger.debug("informações encontradas da senha do usuario: " + login.getUsuario());
				return true;
			}
			pstmt.close();
			result.close();
			
		} catch (SQLException e) {
			logger.error("Erro ao procurar Login do usuario: " + login.getUsuario(), e);
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		logger.debug("senha: " +login.getSenha()+ " não pertence do usuario: " + login.getUsuario());
		return false;

	}

	@Override
	public boolean criaLogin(Login login) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet result = null;
		
		try {
			logger.debug("incluindo login");
			con = Conexao.getConnection();

			pstmt = con.prepareStatement(INSERT_TB_LOGIN_SQL);
			pstmt.setString(1, login.getUsuario());
			pstmt.setString(2, login.getSenha());
			pstmt.executeUpdate();
			
			result = pstmt.getGeneratedKeys();
			if (result.next()){
				login.setId(result.getInt(1));
			}
			result.close();
			pstmt.close();
			logger.debug("login incluido");
			
		} catch (SQLException e) {
			logger.error("Erro ao incluir login", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}
}