package atm.middleware.dao;

import java.util.List;

import atm.model.Cliente;

public interface ClienteDao {
	
	boolean inclui(Cliente umCliente);
	
	boolean altera(Cliente umCliente);
	
	boolean exclui(Cliente umCliente);

	Cliente buscarCliente(long id);
	
	List<Cliente> buscarCliente(String nome);
}
