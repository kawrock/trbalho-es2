package atm.middleware.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import atm.middleware.util.Conexao;
import atm.model.Conta;
import atm.model.Movimentacao;

public class MovimentacaoDaoImpl implements MovimentacaoDao {
	
	// Properties
	
	private static final Logger logger = Logger.getLogger(ClienteDaoImpl.class);
	
	private static final String INSERT_TB_MOVIMENTACAO_SQL = 
			"insert into tb_movimentacao("
					+ "id_cliente, id_cartao, id_conta_a, "
					+ "data, id_conta_b, tipo, valor, "
					+ "entidade, historico) "
			+ "values(?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String SELECT_POR_ID_CONTA_SQL = 
			"select id, data, tipo, valor, entidade "
			+ "from tb_movimentacao "
			+ "where id_conta_a=?";

	// Methods
	
	@Override
	public boolean inclui(Movimentacao umaMovimentacao) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			logger.debug("incluindo movimentacao");
			con = Conexao.getConnection();

			pstmt = con.prepareStatement(INSERT_TB_MOVIMENTACAO_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, (umaMovimentacao.getCliente() != null) ? umaMovimentacao.getCliente().getId() : null);
			
			// TODO Falta alterar
			pstmt.setLong(2, (Long) null);
			pstmt.setLong(3, (umaMovimentacao.getConta_a() != null) ? umaMovimentacao.getConta_a().getId() : null);
			pstmt.setLong(4, (umaMovimentacao.getConta_b() != null) ? umaMovimentacao.getConta_b().getId() : null);
			pstmt.setString(5, umaMovimentacao.getData().toString());
			pstmt.setString(6, umaMovimentacao.getTipo());
			pstmt.setLong(7, umaMovimentacao.getValor());
			pstmt.setString(8, umaMovimentacao.getEntidade());
            pstmt.setString(9, umaMovimentacao.getHistorico());
			
			pstmt.executeUpdate();
			rset = pstmt.getGeneratedKeys();
			if (rset.next()){
				umaMovimentacao.setId(rset.getLong(1));
			}
			rset.close();
			pstmt.close();
			logger.debug("movimentacao incluido");
		} catch (SQLException e) {
			logger.error("Erro ao incluir movimentacao", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public List<Movimentacao> bucarMovimentacaoPorConta(Conta umaConta) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		List<Movimentacao> lista = new ArrayList<Movimentacao>();
		try {
			logger.debug("buscando lista de movimetações");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(SELECT_POR_ID_CONTA_SQL);
			pstmt.setLong(1, umaConta.getId());
			rset = pstmt.executeQuery();
			while (rset.next()) {
				Movimentacao m = new Movimentacao();
				m.setId(rset.getLong("id"));
				m.setData(new Date());
				m.setTipo(rset.getString("tipo"));
				m.setValor(rset.getLong("valor"));
				m.setEntidade(rset.getString("entidade"));
				lista.add(m);
			}
			rset.close();
			pstmt.close();
			logger.debug("lista de movimentações buscada");
		} catch (SQLException e) {
			logger.error("Erro ao buscar lista de movimentações", e);
			throw new RuntimeException("Erro ao buscar lista de movimentações");
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return lista;
	}
}
