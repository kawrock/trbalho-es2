package atm.middleware.dao;

import atm.middleware.util.Conexao;
import atm.model.CartaoDeDebito;
import org.apache.log4j.Logger;

import java.sql.*;

public class CartaoDeDebitoDaoImpl implements CartaoDeDebitoDao {

	// Properties
	
	private static final Logger logger = Logger.getLogger(CartaoDeDebitoDaoImpl.class);
	
	private static final String INSERT_TB_CARTAODEDEBITO_SQL =
			"insert into tb_cartao(numero, validade, nome_cartao, bandeira, identificador, tipo, cod_seguranca, id_cliente) "
			+ "values(?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String DELETE_TB_CARTAODEDEBITO_SQL =
			"delete from tb_cartao where id=?";
        
        private static final String SELECT_SALDO_TB_CARTAODEDEBITO_SQL =
			"select saldo from vw_saldo where conta = ?";

    // Methods
	
	@Override
	public boolean inclui(CartaoDeDebito umCartaoDeDebito) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			logger.debug("Cadastrando cartão de débito");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(INSERT_TB_CARTAODEDEBITO_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, umCartaoDeDebito.getNumero());
			pstmt.setDate(2, new java.sql.Date(umCartaoDeDebito.getDataDeValidade().getTime()));
			pstmt.setString(3, umCartaoDeDebito.getNomeCartao());
			pstmt.setString(4, umCartaoDeDebito.getBandeira());
			pstmt.setInt(5, 100);
			pstmt.setString(6, umCartaoDeDebito.getTipo());
			pstmt.setString(7, umCartaoDeDebito.getCodigoDeSeguranca());
			pstmt.setLong(8, (umCartaoDeDebito.getCliente() != null) ? umCartaoDeDebito.getCliente().getId() : null);
			pstmt.executeUpdate();
			pstmt.close();
			logger.debug("Cartão de débito cadastrado");
		} catch (SQLException e) {
			logger.error("Erro ao cadastrar cartão de débito", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public boolean exclui(CartaoDeDebito umCartaoDeDebito) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			logger.debug("Excluindo cartão de débito");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(DELETE_TB_CARTAODEDEBITO_SQL);
			pstmt.setLong(1, umCartaoDeDebito.getId());
			pstmt.executeUpdate();
			pstmt.close();
			logger.debug("Cartão de débito excluído");
		} catch (SQLException e) {
			logger.error("Erro ao excluir cartão de débito", e);
			return false;
		} finally {
			try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}
        @Override
	public long saldo(CartaoDeDebito umCartaoDeDebito) {
		Connection con;
		PreparedStatement pstmt = null;
		long saldo = 0;
		try {
			logger.debug("Buscando saldo do cartão de débito");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(SELECT_SALDO_TB_CARTAODEDEBITO_SQL);
			pstmt.setLong(1, umCartaoDeDebito.getId());
			ResultSet resultado = pstmt.executeQuery();
			resultado.next();
			saldo = resultado.getLong("Saldo");
			pstmt.close();
			logger.debug("Saldo do cartão de débito encontrado");
			return saldo;
		} catch (SQLException e) {
			logger.error("Erro ao buscar por saldo de cartão de débito", e);
		} finally {
			try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return Integer.MIN_VALUE;
	}
}
