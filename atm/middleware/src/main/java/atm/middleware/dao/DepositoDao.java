package atm.middleware.dao;

import atm.model.Deposito;

public interface DepositoDao {
    
    boolean inclui(Deposito umDeposito);
    
}
