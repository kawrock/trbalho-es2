package atm.middleware.dao;

import atm.model.CartaoDeCredito;

public interface CartaoDeCreditoDao {
	
	boolean inclui(CartaoDeCredito umCartaoDeCredito);
	
	boolean exclui(CartaoDeCredito umCartaoDeCredito);

	long saldo(CartaoDeCredito umCartaoDeCredito);
}
