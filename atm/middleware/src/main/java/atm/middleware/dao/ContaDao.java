package atm.middleware.dao;

import atm.model.Conta;

public interface ContaDao {
	
	boolean inclui(Conta conta);

	boolean exclui(Conta conta);
	
	boolean altera(Conta conta);
	
	Conta buscarConta(String numero);
	
	public Conta buscarConta(long id);
	
	public Conta buscaContaPeloCartao (String numeroCartao);
}
