package atm.middleware.dao;

import atm.middleware.util.Conexao;
import atm.model.CartaoDeCredito;
import org.apache.log4j.Logger;

import java.sql.*;

public class CartaoDeCreditoDaoImpl implements CartaoDeCreditoDao {

	// Properties
	
	private static final Logger logger = Logger.getLogger(CartaoDeCreditoDaoImpl.class);
	
	private static final String INSERT_TB_CARTAODECREDITO_SQL =
			"insert into tb_cartao(numero, validade, nome_cartao, bandeira, identificador, tipo, cod_seguranca, limite, saldo) "
			+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String DELETE_TB_CARTAODECREDITO_SQL =
			"delete from tb_cartao "
					+ "where id=?";

	//mudar esse select para a view que a Mayara vai passar
	private static final String SELECT_SALDO_TB_CARTAODECREDITO_SQL =
			"select saldo from vw_saldo_cartao_credito WHERE conta = ?";

	// Methods
	
	@Override
	public boolean inclui(CartaoDeCredito umCartaoDeCredito) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			logger.debug("cadastrando cartao de crédito");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(INSERT_TB_CARTAODECREDITO_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, umCartaoDeCredito.getNumero());
			pstmt.setDate(2, new java.sql.Date(umCartaoDeCredito.getDataDeValidade().getTime()));
			pstmt.setString(3, umCartaoDeCredito.getNomeCartao());
			pstmt.setString(4, umCartaoDeCredito.getBandeira());
			pstmt.setInt(5, 100); //Que que é identificador???
			pstmt.setString(6, umCartaoDeCredito.getTipo());
			pstmt.setString(7, umCartaoDeCredito.getCodigoDeSeguranca());
			pstmt.setLong(8, umCartaoDeCredito.getLimite());
//			pstmt.setLong(9,umCartaoDeCredito.getSaldo());
			pstmt.executeUpdate();

			rset = pstmt.getGeneratedKeys();
			if (rset.next()){
				umCartaoDeCredito.setId(rset.getLong(1));
			}
			rset.close();
			pstmt.close();
			logger.debug("cartao de crédito cadastrado");
		} catch (SQLException e) {
			logger.error("Erro ao cadastrar cartão de crédito", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public boolean exclui(CartaoDeCredito umCartaoDeCredito) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			logger.debug("excluindo cartao de credito");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(DELETE_TB_CARTAODECREDITO_SQL);
			pstmt.setLong(1, umCartaoDeCredito.getId());
			pstmt.executeUpdate();
			pstmt.close();
			logger.debug("cartao de credito excluido");
		} catch (SQLException e) {
			logger.error("Erro ao excluir cartao de credito", e);
			return false;
		} finally {
			try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public long saldo(CartaoDeCredito umCartaoDeCredito) {
		Connection con;
		PreparedStatement pstmt = null;
		long saldo = 0;
		try {
			logger.debug("buscando saldo do cartao de crédito");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(SELECT_SALDO_TB_CARTAODECREDITO_SQL);
			pstmt.setLong(1, umCartaoDeCredito.getId());
			ResultSet resultado = pstmt.executeQuery();
			resultado.next();
			saldo = resultado.getLong("saldo");
			pstmt.close();
			logger.debug("saldo do cartao de credito encontrado");
			return saldo;
		} catch (SQLException e) {
			logger.error("Erro ao buscar por sado de cartao de credito", e);
		} finally {
			try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return Integer.MIN_VALUE;
	}

}
