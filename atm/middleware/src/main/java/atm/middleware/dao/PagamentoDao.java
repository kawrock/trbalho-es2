package atm.middleware.dao;

import atm.model.Pagamento;

public interface PagamentoDao {
	
	boolean inclui(Pagamento umPagamento);
}
