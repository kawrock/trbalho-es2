package atm.middleware.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import atm.middleware.util.Conexao;
import atm.model.Cliente;

public class ClienteDaoImpl implements ClienteDao {

	// Properties
	
	private static final Logger logger = Logger.getLogger(ClienteDaoImpl.class);
	
	private static final String INSERT_TB_PESSOA_SQL = 
			"insert into tb_pessoa(nome, cpf) "
			+ "values(?, ?)";
	
	private static final String INSERT_TB_CLIENTE_SQL = 
			"insert into tb_cliente(id_pessoa, rg, dt_nascimento, nome_mae, nome_pai, endereco) "
			+ "values(?, ?, ?, ?, ?, ?)" ;

	private static final String UPDATE_TB_PESSOA_SQL = 
			"update tb_pessoa "
			+ "set nome=?, cpf=? "
			+ "where id=?";
	
	private static final String UPDATE_TB_CLIENTE_SQL = 
			"update tb_cliente "
			+ "set rg=?, dt_nascimento=?, nome_mae=?, nome_pai=?, endereco=? "
			+ "where id_pessoa=?";

	private static final String DELETE_TB_PESSOA_SQL = 
			"delete from tb_pessoa "
			+ "where id=?";
	
	private static final String DELETE_TB_CLIENTE_SQL = 
			"delete from tb_cliente "
			+ "where id_pessoa=?";
	
	private static final String SELECT_TB_CLIENTE_ID_SQL = 
			"select * "
			+ "from tb_cliente left outer join tb_pessoa on id_pessoa = id "
			+ "where id = ?";
	
	private static final String SELECT_TB_CLIENTE_NOME_SQL = 
			"select * "
			+ "from tb_cliente left outer join tb_pessoa on id_pessoa = id "
			+ "where nome = ?";
	
	// Methods
	
	@Override
	public boolean inclui(Cliente umCliente) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			logger.debug("incluindo cliente");
			con = Conexao.getConnection();

//			`id` int(11) NOT NULL AUTO_INCREMENT,
//			`nome` varchar(50) DEFAULT NULL,
//			`cpf` varchar(11) DEFAULT NULL,

			pstmt = con.prepareStatement(INSERT_TB_PESSOA_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, umCliente.getNome());
			pstmt.setString(2, umCliente.getCpf());
			pstmt.executeUpdate();
			rset = pstmt.getGeneratedKeys();
			if (rset.next()){
			    umCliente.setId(rset.getLong(1));
			}
			rset.close();
			pstmt.close();
			pstmt = con.prepareStatement(INSERT_TB_CLIENTE_SQL);
			pstmt.setLong(1, umCliente.getId());
			pstmt.setString(2, umCliente.getRg());
//			pstmt.setDate(3, new java.sql.Date(umCliente.getDataDeNascimento().getTime()));
			pstmt.setString(3, "11/09/1995");
			pstmt.setString(4, umCliente.getNomeDaMae());
			pstmt.setString(5, umCliente.getNomeDoPai());
			pstmt.setString(6, umCliente.getEndereco());
			pstmt.executeUpdate();
			logger.debug("cliente incluido");
		} catch (SQLException e) {
			logger.error("Erro ao incluir cliente", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public boolean altera(Cliente umCliente) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			logger.debug("alterando cliente");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(UPDATE_TB_PESSOA_SQL);
			pstmt.setString(1, umCliente.getNome());
			pstmt.setString(2, umCliente.getCpf());
			pstmt.setLong(3, umCliente.getId());
			pstmt.executeUpdate();
			pstmt.close();
			pstmt = con.prepareStatement(UPDATE_TB_CLIENTE_SQL);
//			pstmt.setString(1, umCliente.getRg());
			pstmt.setInt(1, 111111111);
//			pstmt.setDate(2, new java.sql.Date(umCliente.getDataDeNascimento().getTime()));
			pstmt.setDate(2, new java.sql.Date(umCliente.getDataDeNascimento().getTime()));
			pstmt.setString(3, umCliente.getNomeDaMae());
			pstmt.setString(4, umCliente.getNomeDoPai());
			pstmt.setString(5, umCliente.getEndereco());
			pstmt.setLong(6, umCliente.getId());
			pstmt.executeUpdate();
			logger.debug("cliente alterado");
		} catch (SQLException e) {
			logger.error("Erro ao incluir cliente", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public boolean exclui(Cliente umCliente) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			logger.debug("excluindo cliente");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(DELETE_TB_CLIENTE_SQL);
			pstmt.setLong(1, umCliente.getId());
			pstmt.executeUpdate();
			pstmt.close();
			pstmt = con.prepareStatement(DELETE_TB_PESSOA_SQL);
			pstmt.setLong(1, umCliente.getId());
			pstmt.executeUpdate();
			logger.debug("cliente exclu�do");
		} catch (SQLException e) {
			logger.error("Erro ao exclu�do cliente", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public Cliente buscarCliente(long id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		
		Cliente cliente = null;
		
		try
		{
			logger.debug("buscando cliente por id");
			connection = Conexao.getConnection();
			statement = connection.prepareStatement(SELECT_TB_CLIENTE_ID_SQL);
			statement.setLong(1, id);
						
			result = statement.executeQuery();
			
			cliente = new Cliente();
			
			if(result.next())
			{
				cliente.setId(result.getLong("id_pessoa"));
				cliente.setDataDeNascimento(new Date(result.getLong("dt_nascimento")));
				cliente.setEndereco(result.getString("endereco"));
				cliente.setNome(result.getString("nome"));
				cliente.setNomeDaMae(result.getString("nome_mae"));
				cliente.setNomeDoPai(result.getString("nome_pai"));
				cliente.setRg(result.getString("rg"));
				cliente.setCpf(result.getString("cpf"));
			}
			statement.close();
		}
		catch (SQLException e)
		{
			logger.debug("Erro ao buscar cliente por id", e);
			return null;
		}
		finally
		{
			try { if (statement != null) statement.close(); } catch(Exception e) { }
		}
		
		return cliente;
	}
	
	@Override
	public List<Cliente> buscarCliente(String nome) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		
		List<Cliente> clientes = new ArrayList<Cliente>();
		
		try
		{
			logger.debug("buscando clientes por nome");
			connection = Conexao.getConnection();
			statement = connection.prepareStatement(SELECT_TB_CLIENTE_NOME_SQL);
			statement.setString(1, nome);
						
			result = statement.executeQuery();
			
			while(result.next())
			{
				Cliente cliente = new Cliente();
				cliente.setId(result.getLong("id_pessoa"));
				cliente.setDataDeNascimento(new Date(result.getLong("dt_nascimento")));
				cliente.setEndereco(result.getString("endereco"));
				cliente.setNome(result.getString("nome"));
				cliente.setNomeDaMae(result.getString("nome_mae"));
				cliente.setNomeDoPai(result.getString("nome_pai"));
				cliente.setRg(result.getString("rg"));
				cliente.setCpf(result.getString("cpf"));

				clientes.add(cliente);
			}
			
			statement.close();
		}
		catch (SQLException e)
		{
			logger.debug("Erro ao buscar clientes por nome", e);
		}
		finally
		{
			try { if (statement != null) statement.close(); } catch(Exception e) { }
		}
		
		return clientes;
	}
}
