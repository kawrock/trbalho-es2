package atm.middleware.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import atm.middleware.util.Conexao;
import atm.model.Pagamento;

public class PagamentoDaoImpl implements PagamentoDao {
	
	// Properties
	
	private static final Logger logger = Logger.getLogger(ClienteDaoImpl.class);
		
	private static final String INSERT_TB_PAGAMENTO_SQL = 
			"insert into tb_pagamento(codigo_barras, pago) "
			+ "values(?, ?)";

	// Methods
	
	@Override
	public boolean inclui(Pagamento umPagamento) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			logger.debug("incluindo pagamento");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(INSERT_TB_PAGAMENTO_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, umPagamento.getNumeroDoCodigoDeBarras());
			pstmt.setInt(2, 1);
			pstmt.executeUpdate();
			rset = pstmt.getGeneratedKeys();
			if (rset.next()){
				umPagamento.setId(rset.getLong(1));
			}
			rset.close();
			pstmt.close();
			logger.debug("pagamento incluido");
		} catch (SQLException e) {
			logger.error("Erro ao incluir cliente", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

}
