package atm.middleware.dao;

import java.util.List;

import atm.model.Conta;
import atm.model.Movimentacao;

public interface MovimentacaoDao {
	
	boolean inclui(Movimentacao umaMovimentacao);
	
	List<Movimentacao> bucarMovimentacaoPorConta(Conta umaConta);
	
}
