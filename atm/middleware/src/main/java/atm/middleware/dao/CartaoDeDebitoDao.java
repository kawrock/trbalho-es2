package atm.middleware.dao;

import atm.model.CartaoDeDebito;

public interface CartaoDeDebitoDao {
	
	boolean inclui(CartaoDeDebito umCartaoDeDebito);
	
	boolean exclui(CartaoDeDebito umCartaoDeDebito);
        
        long saldo(CartaoDeDebito umCartaoDeDebito);

}
