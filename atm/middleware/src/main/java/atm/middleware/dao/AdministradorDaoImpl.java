package atm.middleware.dao;

import atm.middleware.util.Conexao;
import atm.model.Administrador;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AdministradorDaoImpl implements AdministradorDao {

	// Properties
	
	private static final Logger logger = Logger.getLogger(AdministradorDaoImpl.class);
	
	private static final String INSERT_TB_PESSOA_SQL = 
			"insert into tb_pessoa(nome, cpf) "
			+ "values(?, ?)";
	
	private static final String INSERT_TB_ADMINISTRADOR_SQL =
			"insert into tb_administrador(id_pessoa, codigo, cargo) "
			+ "values(?, ?, ?)" ;

	private static final String UPDATE_TB_PESSOA_SQL = 
			"update tb_pessoa "
			+ "set nome=?, cpf=? "
			+ "where id=?";
	
	private static final String UPDATE_TB_ADMINISTRADOR_SQL =
			"update tb_administrador "
			+ "set codigo=?, cargo=?"
			+ "where id_pessoa=?";

	private static final String DELETE_TB_PESSOA_SQL = 
			"delete from tb_pessoa "
			+ "where id=?";
	
	private static final String DELETE_TB_ADMINISTRADOR_SQL =
			"delete from tb_administrador "
			+ "where id_pessoa=?";
	
	private static final String SELECT_TB_ADMINISTRADOR_ID_SQL =
			"select * "
			+ "from tb_administrador left outer join tb_pessoa on id_pessoa = id "
			+ "where id = ?";
	
	private static final String SELECT_TB_ADMINISTRADOR_NOME_SQL =
			"select * "
			+ "from tb_administrador left outer join tb_pessoa on id_pessoa = id "
			+ "where nome = ?";
	
	// Methods
	
	@Override
	public boolean inclui(Administrador umAdministrador) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			logger.debug("incluindo administrador");
			con = Conexao.getConnection();

			pstmt = con.prepareStatement(INSERT_TB_PESSOA_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, umAdministrador.getNome());
			pstmt.setString(2, umAdministrador.getCpf());
			pstmt.executeUpdate();
			rset = pstmt.getGeneratedKeys();
			if (rset.next()){
				umAdministrador.setId(rset.getLong(1));
			}
			rset.close();
			pstmt.close();
			pstmt = con.prepareStatement(INSERT_TB_ADMINISTRADOR_SQL);
			pstmt.setLong(1, umAdministrador.getId());
			pstmt.setString(2, umAdministrador.getCodigoIdentificador());
			pstmt.setString(3, umAdministrador.getCargo());
			pstmt.executeUpdate();
			logger.debug("administrador incluido");
		} catch (SQLException e) {
			logger.error("Erro ao incluir administrador", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public boolean altera(Administrador umAdministrador) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			logger.debug("alterando administrador");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(UPDATE_TB_PESSOA_SQL);
			pstmt.setString(1, umAdministrador.getNome());
			pstmt.setString(2, umAdministrador.getCpf());
			pstmt.setLong(3, umAdministrador.getId());
			pstmt.executeUpdate();
			pstmt.close();
			pstmt = con.prepareStatement(UPDATE_TB_ADMINISTRADOR_SQL);
			pstmt.setString(1, umAdministrador.getCodigoIdentificador());
			pstmt.setString(2, umAdministrador.getCargo());
			pstmt.setLong(3, umAdministrador.getId());
			pstmt.executeUpdate();
			logger.debug("administrador alterado");
		} catch (SQLException e) {
			logger.error("Erro ao alterar administrador", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public boolean exclui(Administrador umAdministrador) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			logger.debug("excluindo administrador");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(DELETE_TB_ADMINISTRADOR_SQL);
			pstmt.setLong(1, umAdministrador.getId());
			pstmt.executeUpdate();
			pstmt.close();
			pstmt = con.prepareStatement(DELETE_TB_PESSOA_SQL);
			pstmt.setLong(1, umAdministrador.getId());
			pstmt.executeUpdate();
			logger.debug("administrador excluido");
		} catch (SQLException e) {
			logger.error("Erro ao excluir administrador", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public Administrador buscarAdministrador(long id){
	Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		
		Administrador umAdministrador = null;
		
		try
		{
			logger.debug("buscando administrador");
			connection = Conexao.getConnection();
			statement = connection.prepareStatement(SELECT_TB_ADMINISTRADOR_ID_SQL);
			statement.setLong(1, id);
						
			result = statement.executeQuery();

			umAdministrador = new Administrador();
			
			if(result.next())
			{
				umAdministrador.setId(result.getLong("id_pessoa"));
				umAdministrador.setCodigoIdentificador(result.getString("codigo"));
				umAdministrador.setCargo(result.getString("cargo"));
				umAdministrador.setNome(result.getString("nome"));
				umAdministrador.setCpf(result.getString("cpf"));
			}
			statement.close();
		}
		catch (SQLException e)
		{
			logger.debug("Erro ao buscar administrador", e);
			return null;
		}
		finally
		{
			try { if (statement != null) statement.close(); } catch(Exception e) { }
		}
		
		return umAdministrador;
	}
	
	@Override
	public List<Administrador> buscarAdministrador(String nome) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		
		List<Administrador> administradores = new ArrayList<Administrador>();
		
		try
		{
			logger.debug("buscando administradores");
			connection = Conexao.getConnection();
			statement = connection.prepareStatement(SELECT_TB_ADMINISTRADOR_NOME_SQL);
			statement.setString(1, nome);
						
			result = statement.executeQuery();
			
			while(result.next())
			{
				Administrador administrador = new Administrador();
				administrador.setId(result.getLong("id_pessoa"));
				administrador.setCodigoIdentificador(result.getString("codigo"));
				administrador.setCargo(result.getString("cargo"));
				administrador.setNome(result.getString("nome"));
				administrador.setCpf(result.getString("cpf"));
				administradores.add(administrador);
			}
			statement.close();
		}
		catch (SQLException e)
		{
			logger.debug("Erro ao buscar administradores", e);
		}
		finally
		{
			try { if (statement != null) statement.close(); } catch(Exception e) { }
		}
		
		return administradores;
	}




}
