package atm.middleware.dao;

import atm.model.Login;

public interface LoginDao {
	
	boolean validaUsuario(Login login);
	
	boolean validaSenha(Login login);
	
	boolean criaLogin(Login login);
}
