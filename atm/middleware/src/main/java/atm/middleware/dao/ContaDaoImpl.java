package atm.middleware.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import atm.middleware.util.Conexao;
import atm.model.Conta;

public class ContaDaoImpl implements ContaDao {

	// Properties
	
	private static final Logger logger = Logger.getLogger(ContaDaoImpl.class);
	
	private static final String INSERT_TB_CONTA_SQL = 
			"insert into tb_conta(num_conta, agencia, id_cliente, tipo, saldo) "
			+ "values(?, ?, ?, ?, ?)";
	
	private static final String DELETE_TB_CONTA_SQL =
			"delete from tb_conta "
			+ "where id=?";
	
	private static final String SELECT_TB_CONTA_ID_SQL = 
			"select * from tb_conta "
			+ "where id=?";
	
	private static final String SELECT_TB_CONTA_NUMERO_SQL =
			"select * from tb_conta "
			+ "where num_conta=?";
	
	private static final String SELECT_TB_CONTA_CARTAO_SQL = 
			"select id_conta from tb_cartao join tb_conta_cartao "
			+ "where numero=?";
	
	private static final String UPDATE_TB_CONTA_AGENCIA_SQL = 
			"update tb_conta "
			+ "set agencia=?, num_conta=?, saldo=?, tipo=?"
			+ "where id=?";
	
	// Methods
	
	@Override
	public boolean inclui(Conta conta) 
	{
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			logger.debug("incluindo conta");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(INSERT_TB_CONTA_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, conta.getNumero());
			pstmt.setString(2, conta.getAgencia());
//			pstmt.setLong(3, conta.getClienteID());
			pstmt.setLong(3, (conta.getProprietario() != null) ? conta.getProprietario().getId() : null);
			pstmt.setString(4, conta.getTipo());
			pstmt.setLong(5, conta.getSaldo());
			pstmt.executeUpdate();
			pstmt.close();

			logger.debug("conta incluida");
		} catch (SQLException e) {
			logger.error("Erro ao incluir conta", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}

	@Override
	public boolean exclui(Conta conta) 
	{
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			logger.debug("excluindo conta");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(DELETE_TB_CONTA_SQL);
			pstmt.setLong(1, conta.getId());
			pstmt.executeUpdate();
			pstmt.close();
			logger.debug("conta excluida");
		} catch (SQLException e) {
			logger.error("Erro ao excluir conta", e);
			return false;
		} finally {
				try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}
	
	@Override
	public boolean altera(Conta conta) 
	{
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			logger.debug("atualizando conta");
			con = Conexao.getConnection();
			pstmt = con.prepareStatement(UPDATE_TB_CONTA_AGENCIA_SQL);
			pstmt.setString(1, conta.getAgencia());
			pstmt.setString(2, conta.getNumero());
			pstmt.setLong(3, conta.getSaldo());
			pstmt.setString(4, conta.getTipo());
			pstmt.setLong(5, conta.getId());
			pstmt.executeUpdate();
			pstmt.close();
			logger.debug("conta atualizada");
		} catch (SQLException e) {
			logger.error("Erro ao atualizar conta", e);
			return false;
		} finally {
				try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}
	
	@Override
	public Conta buscarConta(long id)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		
		Conta conta = null;
		
		try
		{
			logger.debug("buscando conta pela id");
			connection = Conexao.getConnection();
			statement = connection.prepareStatement(SELECT_TB_CONTA_ID_SQL);
			statement.setLong(1, id);
						
			result = statement.executeQuery();
			
			if (result.next())
			{
				conta = new Conta(result.getLong("id_cliente"));
				conta.setNumero(result.getString("num_conta"));
				conta.setAgencia(result.getString("agencia"));
				conta.setId(result.getLong("id"));
				conta.setTipo(result.getString("tipo"));
			}
			else{
				logger.debug("Deu ruim kelly");
			}
			statement.close();
			if (conta == null) throw new RuntimeException("A conta não existe");
		}
		catch (SQLException e)
		{
			logger.debug("Erro ao buscar conta", e);
			return null;
		}
		finally
		{
			try { if (statement != null) statement.close(); } catch(Exception e) { }
		}
		
		return conta;
	}
	
	@Override
	public Conta buscarConta(String numero)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		
		Conta conta = null;
		
		try
		{
			logger.debug("buscando conta");
			connection = Conexao.getConnection();
			statement = connection.prepareStatement(SELECT_TB_CONTA_NUMERO_SQL);
			statement.setString(1, numero);
						
			result = statement.executeQuery();
			
			if (result.next())
			{
				conta = new Conta(result.getLong("id_cliente"));
				conta.setNumero(result.getString("num_conta"));
				conta.setAgencia(result.getString("agencia"));
				conta.setId(result.getLong("id"));
				conta.setTipo(result.getString("tipo"));
			}
			statement.close();
			if (conta == null) throw new RuntimeException("A conta não existe");
		}
		catch (SQLException e)
		{
			logger.debug("Erro ao buscar conta", e);
			return null;
		}
		finally
		{
			try { if (statement != null) statement.close(); } catch(Exception e) { }
		}
		
		return conta;
	}
	
	@Override
	public Conta buscaContaPeloCartao (String numeroCartao){
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		
		long id_conta = 0;
		
		try
		{
			logger.debug("buscando cartão");
			connection = Conexao.getConnection();
			statement = connection.prepareStatement(SELECT_TB_CONTA_CARTAO_SQL);
			statement.setString(1, numeroCartao);
			
			result = statement.executeQuery();
			
			if (result.next())
			{
				id_conta = result.getLong("id_conta");
			}
			statement.close();
			if (id_conta == 0) throw new RuntimeException("O cartão nao existe");
		}
		catch (SQLException e)
		{
			logger.debug("Erro ao buscar conta", e);
			return null;
		}
		finally
		{
			try { if (statement != null) statement.close(); } catch(Exception e) { }
		}
		
		return buscarConta(id_conta);
	}
}
