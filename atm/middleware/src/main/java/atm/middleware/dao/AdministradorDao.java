package atm.middleware.dao;

import atm.model.Administrador;

import java.util.List;

public interface AdministradorDao {
	
	boolean inclui(Administrador umAdministrador);
	
	boolean altera(Administrador umAdministrador);
	
	boolean exclui(Administrador umAdministrador);

	Administrador buscarAdministrador(long id);
	
	List<Administrador> buscarAdministrador(String nome);
}
