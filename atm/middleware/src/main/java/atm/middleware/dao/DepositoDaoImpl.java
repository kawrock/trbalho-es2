package atm.middleware.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import atm.middleware.util.Conexao;
import atm.model.Deposito;

public class DepositoDaoImpl implements DepositoDao {
	
	// Properties
	
	private static final Logger logger = Logger.getLogger(ClienteDaoImpl.class);
	
	private static final String INSERT_TB_DEPOSITO_SQL = 
			"insert into tb_deposito("
					+ "id_conta_a, "
					+ "id_conta_b, id_cheque, valor, "
					+ "data) "
			+ "values(?, ?, ?, ?, ?)";

	// Methods
	
	@Override
	public boolean inclui(Deposito umDeposito) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			logger.debug("incluindo deposito");
			con = Conexao.getConnection();

			pstmt = con.prepareStatement(INSERT_TB_DEPOSITO_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, (umDeposito.getContaDeOrigem() != null) ? umDeposito.getContaDeOrigem().getId() : null);
                        pstmt.setLong(2, (umDeposito.getContaDeDestino() != null) ? umDeposito.getContaDeDestino().getId() : null);
			pstmt.setLong(3, 111);
			pstmt.setLong(4, umDeposito.getValor());
                        pstmt.setDate(5, new java.sql.Date(umDeposito.getData().getTime()));
			
			pstmt.executeUpdate();
			pstmt.close();
			logger.debug("deposito incluido");
		} catch (SQLException e) {
			logger.error("Erro ao incluir deposito", e);
			return false;
		} finally {
	 		try { if (pstmt != null) pstmt.close(); } catch(Exception e) { }
		}
		return true;
	}
}
