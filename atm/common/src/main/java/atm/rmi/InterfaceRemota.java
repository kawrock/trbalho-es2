package atm.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import atm.model.Administrador;
import atm.model.CartaoDeCredito;
import atm.model.Cliente;
import atm.model.CartaoDeDebito;
import atm.model.Conta;

public interface InterfaceRemota extends Remote {
	
	boolean incluiCliente(Cliente umCliente) throws RemoteException;

	boolean alteraCliente(Cliente umCliente) throws RemoteException;
	
	boolean excluiCliente(Cliente umCliente) throws RemoteException;

	boolean incluiConta(Conta conta) throws RemoteException;
	
	boolean alteraConta(Conta conta) throws RemoteException;
	
	boolean excluiConta(Conta conta) throws RemoteException;

	Conta buscarConta(String numero) throws RemoteException;
	
	Conta buscarContaPeloCartao(String numeroCartao) throws RemoteException;

	Cliente buscarCliente(long id) throws RemoteException;

	List<Cliente> buscarClientes(String nome) throws RemoteException;

	boolean incluiCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) throws RemoteException;

	boolean excluiCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) throws RemoteException;

	long saldoCartaoDeCredito(CartaoDeCredito umCartaoDeCredito) throws RemoteException;

	boolean incluiAdministrador(Administrador umAdministrador);

	boolean alteraAdministrador(Administrador umAdministrador);

	boolean excluiAdministrador(Administrador umAdministrador);
        
        boolean incluiCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) throws RemoteException;

	boolean excluiCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) throws RemoteException;
        
        long saldoCartaoDeDebito(CartaoDeDebito umCartaoDeDebito) throws RemoteException;
}
