package atm.model;

import java.io.Serializable;

public class Transferencia implements Serializable {
	
	// Properties
	
	private static final long serialVersionUID = 1L;

	private Conta contaDeOrigem;
	
	private Conta contaDeDestino;
	
	private Long valor;
	
	// Constructors
	
	public Transferencia() {
		
	}
	
	// Get

	public Conta getContaDeOrigem() {
		return contaDeOrigem;
	}

	public Conta getContaDeDestino() {
		return contaDeDestino;
	}

	public Long getValor() {
		return valor;
	}

	// Set

	public void setContaDeOrigem(Conta contaDeOrigem) {
		this.contaDeOrigem = contaDeOrigem;
	}

	public void setContaDeDestino(Conta contaDeDestino) {
		this.contaDeDestino = contaDeDestino;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}
	
}
