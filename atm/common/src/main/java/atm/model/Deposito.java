package atm.model;

import java.io.Serializable;
import java.util.Date;

public class Deposito implements Serializable {
	
	// Properties
	
	private static final long serialVersionUID = 1L;

	private Conta contaDeOrigem;
        
    private Conta contaDeDestino;
	
	private Long valor;
        
    private Date data;
        
    private String cheque;

	
	// Constructors
	
	public Deposito() {
		
	}
	
	// Get

	public Conta getContaDeOrigem() {
		return contaDeOrigem;
	}
        
    public Conta getContaDeDestino() {
	return contaDeDestino;
	}

	public Long getValor() {
		return valor;
	}
        
    public Date getData() {
    return data;
    }
    public String getCheque() {
	return cheque;
	}

	// Set

	public void setContaDeOrigem(Conta contaDeOrigem) {
		this.contaDeOrigem = contaDeOrigem;
	}
        
    public void setContaDeDestino(Conta contaDeDestino) {
	this.contaDeDestino = contaDeDestino;
    }

	public void setValor(Long valor) {
		this.valor = valor;
	}
        
    public void setData(Date data) {
    this.data = data;
    }
    
    public void setCheque(String cheque) {
    this.cheque = cheque;
    }
}

