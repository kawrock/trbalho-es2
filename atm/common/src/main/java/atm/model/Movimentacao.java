package atm.model;

import java.io.Serializable;
import java.util.Date;

public class Movimentacao implements Serializable {

	// Constants
	
	private static final long serialVersionUID = 1L;
	
	public static final String TRANSFERENCIA = "Transferência";
        
    public static final String DEPOSITO = "Depósito";
    
    public static final String PAGAMENTO = "Pagamento";

	// Properties
	
	private Long id;
	
	private Date data;
	
	private String tipo;
	
	private Long valor;
	
	private String entidade;
	
	private Cliente cliente;
	
	private Conta conta_a;
        
    private Conta conta_b;
        
    private String historico;
	
	// Constructors

	public Movimentacao() {
		
	}

	// Get
	
	public Long getId() {
		return id;
	}

	public Date getData() {
		return data;
	}

	public String getTipo() {
		return tipo;
	}

	public Long getValor() {
		return valor;
	}

	public String getEntidade() {
		return entidade;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public Conta getConta_a() {
		return conta_a;
	}
        
   public Conta getConta_b() {
		return conta_b;
	}
        
    public String getHistorico() {
		return historico;
	}

	// Set

	public void setId(Long id) {
		this.id = id;
	}
	
	public void setData(Date data) {
		this.data = data;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}

	public void setEntidade(String entidade) {
		this.entidade = entidade;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setConta_a(Conta conta) {
		this.conta_a = conta;
	}
        
        public void setConta_b(Conta conta) {
		this.conta_b = conta;
	}
        
        public void setHistorico(String historico) {
		this.historico = historico;
	}
}
