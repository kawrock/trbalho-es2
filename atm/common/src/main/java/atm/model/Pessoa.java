package atm.model;

import java.io.Serializable;

public abstract class Pessoa implements Serializable {
	
	// Properties
	
	private static final long serialVersionUID = 1L;

	protected Long id;
	
	protected String nome;
	
	protected String cpf;
	
	// Constructors
	
	public Pessoa() {
		
	}

	// Get
	
	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getCpf() {
		return cpf;
	}

	// Set
	
	public void setId(Long id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
