package atm.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class Conta implements Serializable
{
	private static final long serialVersionUID = 1L;

	private long id;
//	
//	private long saldo;
//	
	private String tipo;
	
	private String agencia;
	
	private String numero;
	
//	private long id_proprietario;
	
	private Cliente proprietario;

	private List<Movimentacao> movimentacoes;
	
	public Conta() {
		
	}
	
	public Conta(long id)
	{
		this(id, "", "", "", 0);
	}
	
	public Conta(long id_proprietario, String agencia, String numero, String tipo, long saldo)
	{
//		this.id_proprietario = id_proprietario;
		this.proprietario = new Cliente();
		this.proprietario.setId(id_proprietario);
		this.agencia = agencia;
		this.numero = numero;
		this.tipo = tipo;
//		this.saldo = saldo;
	}
	
	public void setAgencia(String agencia)
	{
		this.agencia = agencia;
	}
	
	public void setNumero(String numero)
	{
		this.numero = numero;
	}
//	
//	public void setCliente(Cliente proprietario)
//	{
//		this.id_proprietario = proprietario.getId();
//	}
//	
	public void setId(long id)
	{
		this.id = id;
	}
//	
//	public void setSaldo(long saldo)
//	{
//		this.saldo = saldo;
//	}
//	
	public void setTipo(String tipo)
	{
		this.tipo = tipo;
	}

	public void setMovimentacoes(List<Movimentacao> movimentacoes) {
		this.movimentacoes = movimentacoes;
	}

	public void setProprietario(Cliente proprietario) {
		this.proprietario = proprietario;
	}
//
//	public long getSaldo()
//	{
//		return saldo;
//	}
//
//	public long getClienteID() 
//	{
//		return id_proprietario;
//	}

	public String getAgencia() 
	{
		return agencia;
	}

	public String getNumero() {
		return numero;
	}

	public long getId() {
		return id;
	}
	
	public String getTipo()
	{
		return tipo;
	}

	public Cliente getProprietario() {
		return proprietario;
	}

	public List<Movimentacao> getMovimentacoes() {
		return movimentacoes;
	}

	public Long getSaldo() {
		long saldo = 0L;
		for (Movimentacao m : movimentacoes) {
			saldo += m.getValor();
		}
		return saldo;
	}

	public List<Movimentacao> getExtrato(int dias) {
		// Configura a data de comparacao considerando data, hora e milessegundos
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		gc.set(GregorianCalendar.MILLISECOND, 0);
		gc.add(GregorianCalendar.DAY_OF_MONTH, dias*-1);
		List<Movimentacao> extrato = new ArrayList<Movimentacao>();
		for (Movimentacao m : movimentacoes) {
			if (gc.getTime().before(m.getData())) extrato.add(m);
		}
		return extrato;
	}
}
