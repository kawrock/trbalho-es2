package atm.model;

import java.io.Serializable;

public class Pagamento implements Serializable {

	// Properties

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String numeroDoCodigoDeBarras;
	
	// Construtores
	
	public Pagamento() {
		
	}

	// Get

	public Long getId() {
		return id;
	}

	public String getNumeroDoCodigoDeBarras() {
		return numeroDoCodigoDeBarras;
	}

	// Set

	public void setId(Long id) {
		this.id = id;
	}

	public void setNumeroDoCodigoDeBarras(String numeroDoCodigoDeBarras) {
		this.numeroDoCodigoDeBarras = numeroDoCodigoDeBarras;
	}
	
}
