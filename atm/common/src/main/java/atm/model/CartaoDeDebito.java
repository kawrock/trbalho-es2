package atm.model;

import java.io.Serializable;
import java.util.Date;

public class CartaoDeDebito implements Serializable {

    // Properties
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
    
    private String numero;

    private Date dataDeValidade;
    
    private String nomeCartao;
    
    private String bandeira;
    
    private int identificador;
    
    private final String tipo = "credito";
    
    private String codigoDeSeguranca;

    private long limite;

    private Cliente cliente;

    
    public CartaoDeDebito(){
    }
    
    public long getId() {
        return id;
    }
    
    public String getNumero() {
        return numero;
    }
    
    public Date getDataDeValidade() {
        return dataDeValidade;
    }
    
    public String getNomeCartao() {
        return nomeCartao;
    }
    
    public String getBandeira() {
        return bandeira;
    }
    
    public int getIdentificador() {
        return identificador;
    }
    
    public String getTipo() {
        return tipo;
    }
    
    public String getCodigoDeSeguranca() {
        return codigoDeSeguranca;
    }
    
    public long getLimite() {
        return limite;
    }

    public Cliente getCliente() {
        return cliente;
    }
    
     public void setNumero(String numero) {
        this.numero = numero;
    }
    
    public void setDataDeValidade(Date dataDeValidade) {
        this.dataDeValidade = dataDeValidade;
    }
    
    public void setNomeCartao(String nomeCartao) {
        this.nomeCartao = nomeCartao;
    }
    
    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public long setIdentidicador() {
        return identificador;
    }
    
    public void setCodigoDeSeguranca(String codigoDeSeguranca) {
        this.codigoDeSeguranca = codigoDeSeguranca;
    }    

    public void setLimite(long limite) {
        this.limite = limite;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
