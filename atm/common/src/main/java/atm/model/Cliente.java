package atm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Cliente extends Pessoa implements Serializable {
	
	// Properties

	private static final long serialVersionUID = 1L;

	private String rg;
	
	private Date dataDeNascimento;
	
	private String nomeDoPai;
	
	private String nomeDaMae;
	
	private String endereco;

	private List<CartaoDeCredito> cartoes;

	// Constructor
	
	public Cliente() {
		super();
	}
	
	// Get

	public String getRg() {
		return rg;
	}

	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}

	public String getNomeDoPai() {
		return nomeDoPai;
	}

	public String getNomeDaMae() {
		return nomeDaMae;
	}

	public String getEndereco() {
		return endereco;
	}

	public List<CartaoDeCredito> getCartoes() {
		return cartoes;
	}


	// Set
	
	public void setRg(String rg) {
		this.rg = rg;
	}

	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public void setNomeDoPai(String nomeDoPai) {
		this.nomeDoPai = nomeDoPai;
	}

	public void setNomeDaMae(String nomeDaMae) {
		this.nomeDaMae = nomeDaMae;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setCartoes(List<CartaoDeCredito> cartoes) {
		this.cartoes = cartoes;
	}

}
