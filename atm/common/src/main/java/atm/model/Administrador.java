package atm.model;

import java.io.Serializable;
import java.util.List;

public class Administrador extends Pessoa implements Serializable {

	// Properties

	private static final long serialVersionUID = 1L;

	private String cargo;

	private String codigoIdentificador;

	private List<CartaoDeCredito> cartoes;

	// Constructor

	public Administrador() {
		super();
	}
	
	// Get



	public String getCargo() {
		return cargo;
	}

	public String getCodigoIdentificador() {
		return codigoIdentificador;
	}



	// Set

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public void setCodigoIdentificador(String codigoIdentificador) {
		this.codigoIdentificador = codigoIdentificador;
	}


}
