package atm.lipermi;

import java.util.List;

import atm.model.Conta;
import atm.model.Pagamento;
import atm.model.Transferencia;
import atm.model.Deposito;
import atm.model.Movimentacao;

public interface FrontendService {
	
	Conta buscarContaPeloCartao(String numeroCartao);
	
	long saldoConta (Conta conta);
	
	List<Movimentacao> extratoConta (Conta conta, int dias);
	
	boolean efetuaUmPagamento(Pagamento umPagamento);
	
	boolean efetuaUmaTransferencia(Transferencia umaTransferencia);
        
    boolean efetuaUmDeposito(Deposito umDeposito);
    
}
