package com.atm.kawrock.telas_atm;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

public class FragmentTelaDeposito extends Fragment {

    ComunicacaoFragmentActivity mCallback;
    View view;
    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tela_deposito, null);
        activity = getActivity();
        iniciarBotoesFuncao();
        iniciarBotoesLateral();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (ComunicacaoFragmentActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ComunicacaoFragmentActibity");
        }
    }

    public void iniciarBotoesFuncao(){
        Button botaoDepositoDinheiro = (Button) view.findViewById(R.id.bt_deposito_dinheiro);
        botaoDepositoDinheiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_deposito_dinheiro);
            }
        });
        Button botaoDepositoCheque = (Button) view.findViewById(R.id.bt_deposito_cheque);
        botaoDepositoCheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_deposito_cheque);
            }
        });
    }

    public void iniciarBotoesLateral() {

        ImageButton botaoEsquerdo1 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_1);
        botaoEsquerdo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_deposito_dinheiro);
            }
        });
        ImageButton botaoEsquerdo2 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_2);
        botaoEsquerdo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoEsquerdo3 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_3);
        botaoEsquerdo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoEsquerdo4 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_4);
        botaoEsquerdo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });

        ImageButton botaoDireito1 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_1);
        botaoDireito1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_deposito_cheque);
            }
        });
        ImageButton botaoDireito2 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_2);
        botaoDireito2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoDireito3 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_3);
        botaoDireito3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoDireito4 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_4);
        botaoDireito4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
    }
}