package com.atm.kawrock.telas_atm;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import net.sf.lipermi.handler.CallHandler;
import net.sf.lipermi.net.Client;

import org.w3c.dom.Text;

import java.io.IOException;

import atm.lipermi.FrontendService;
import atm.model.Cliente;

public class FragmentSaldo extends Fragment {
    ComunicacaoFragmentActivity mCallback;

    View view;
    Activity activity;
    private int telaQueChamou;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_saldo, null);
        activity = getActivity();

        iniciarBotoesLateral();
        iniciaTextView();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (ComunicacaoFragmentActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ComunicacaoFragmentActibity");
        }
    }

    public void setTelaQueChamou (int telaQueChamou){
        this.telaQueChamou = telaQueChamou;
    }

    public void iniciaTextView(){
        TextView tvSaldo = (TextView) view.findViewById(R.id.tv_valor_saldo);
        if (telaQueChamou == R.string.tela_cartao_credito){

        } else if (telaQueChamou == R.string.tela_conta_corrente){
            new ConnSaldo(tvSaldo).execute();
        } else if (telaQueChamou == R.string.tela_poupanca){

        }

    }

    public void iniciarBotoesLateral() {

        ImageButton botaoEsquerdo1 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_1);
        botaoEsquerdo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoEsquerdo2 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_2);
        botaoEsquerdo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoEsquerdo3 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_3);
        botaoEsquerdo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoEsquerdo4 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_4);
        botaoEsquerdo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });

        ImageButton botaoDireito1 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_1);
        botaoDireito1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoDireito2 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_2);
        botaoDireito2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoDireito3 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_3);
        botaoDireito3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoDireito4 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_4);
        botaoDireito4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
    }
}

class ConnSaldo extends AsyncTask<Void, Void, String> {

    private TextView tvSaldo;

    ConnSaldo (TextView tvSaldo){
        this.tvSaldo = tvSaldo;
    }

    @Override
    protected String doInBackground(Void... params) {
        //Looper.prepare();
        long valorSaldo = 0;
        try {
            CallHandler callHandler = new CallHandler();
            Client client = new Client(MainActivity.serverIp, 7777, callHandler);
            FrontendService frontendService = (FrontendService) client.getGlobal(FrontendService.class);
            valorSaldo = frontendService.saldoConta(MainActivity.conta);
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Looper.loop();
        return "R$ " + valorSaldo/100f;
    }

    protected void onPostExecute(String saldo) {
        tvSaldo.setText(saldo);
    }
}