package com.atm.kawrock.telas_atm;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import atm.model.Conta;

public class MainActivity extends FragmentActivity implements ComunicacaoFragmentActivity{
    public static CustomKeyboard mCustomKeyboard;
    public static int senha = 1234;
    public static String login = "123";
    public static Conta conta;
    public static String serverIp = "192.168.173.1";
    FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_atm);

        mCustomKeyboard = new CustomKeyboard(this, R.id.keyboardview, R.xml.teclado_atm);

        fm = getSupportFragmentManager();
        if (savedInstanceState == null) {
            FragmentTelaInicial telaInicial = new FragmentTelaInicial();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fragment_container, telaInicial);
            ft.commit();
        }
    }

    public void trocaTela(int telaASerAberta) {
        FragmentTransaction ft;
        switch (telaASerAberta) {
            case R.string.tela_inicial:
                FragmentTelaInicial telaInicial = new FragmentTelaInicial();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaInicial, "telaInicial");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;

            case R.string.tela_login:
                FragmentTelaLogin telaLogin = new FragmentTelaLogin();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaLogin, "telaLogin");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_deposito:
                FragmentTelaDeposito telaDeposito = new FragmentTelaDeposito();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaDeposito, "telaDeposito");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;

            case R.string.tela_deposito_cheque:
                FragmentTelaDepositoCheque telaDepositoCheque = new FragmentTelaDepositoCheque();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaDepositoCheque, "telaDepositoCheque");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_deposito_dinheiro:
                FragmentTelaDepositoDinheiro telaDepositoDinheiro = new FragmentTelaDepositoDinheiro();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaDepositoDinheiro, "telaDepositoDinheiro");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;

            case R.string.tela_home:
                FragmentTelaHome telaHome = new FragmentTelaHome();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaHome, "telaHome");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_conta_corrente:
                FragmentTelaContaCorrente telaContaCorrente = new FragmentTelaContaCorrente();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaContaCorrente, "telaContaCorrente");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_cartao_credito:
                FragmentTelaCartaoCredito telaCartaoCredito = new FragmentTelaCartaoCredito();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaCartaoCredito, "telaCartaoCredito");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_investimentos:
                FragmentTelaInvestimentos telaInvestimentos = new FragmentTelaInvestimentos();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaInvestimentos, "telaInvestimentos");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_pagamento:
                FragmentTelaPagamento telaPagamento = new FragmentTelaPagamento();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaPagamento, "telaPagamento");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_transferencia:
                FragmentTelaTransferencia telaTransferencia = new FragmentTelaTransferencia();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaTransferencia, "telaTransferencia");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;

            case R.string.tela_saldo_conta_corrente:
                FragmentSaldo telaSaldo = new FragmentSaldo();
                telaSaldo.setTelaQueChamou(R.string.tela_conta_corrente);
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaSaldo, "telaSaldoContaCorrente");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_saque:
                break;
            case R.string.tela_extrato_conta_corrente:
                FragmentExtrato telaExtratoConta = new FragmentExtrato();
                telaExtratoConta.setTelaQueChamou(R.string.tela_conta_corrente);
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaExtratoConta, "telaExtratoConta");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_cheque:
                FragmentTelaCheque telaCheque = new FragmentTelaCheque();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaCheque, "telaCheque");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;

            case R.string.tela_extrato_cartao_credito:
                break;
            case R.string.tela_saldo_cartao_credito:
                FragmentSaldo telaSaldoCartao = new FragmentSaldo();
                telaSaldoCartao.setTelaQueChamou(R.string.tela_cartao_credito);
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaSaldoCartao, "telaSaldoCartaoCredito");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;

            case R.string.tela_poupanca:
                FragmentTelaPoupanca telaPoupanca = new FragmentTelaPoupanca();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, telaPoupanca, "telaPopanca");
                ft.addToBackStack(getString(R.string.pilha));
                ft.commit();
                break;
            case R.string.tela_extrato_poupanca:
                break;
            case R.string.tela_saldo_poupanca:
                break;
            case R.string.tela_aplicacao:
                break;
            case R.string.tela_resgate:
                break;

            case R.string.tela_boleto_bancario:
                break;
            case R.string.tela_convenio:
                break;
            case R.string.tela_fatura_cartao_credito:
                break;

            case R.string.tela_impressao_cheque:

        }
    }
    public void confirmaSenha (int telaASerAberta){
        FragmentTelaConfirmacaoSenha telaConfirmacaoSenha = new FragmentTelaConfirmacaoSenha();
        telaConfirmacaoSenha.setTelaASerAberta(telaASerAberta);
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_container, telaConfirmacaoSenha, "telaConfirmacaoSenha");
        ft.addToBackStack(getString(R.string.pilha));
        ft.commit();
    }
}