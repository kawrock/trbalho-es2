package com.atm.kawrock.telas_atm;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import net.sf.lipermi.handler.CallHandler;
import net.sf.lipermi.net.Client;

import java.io.IOException;

import atm.lipermi.FrontendService;
import atm.model.Conta;

public class FragmentTelaLogin extends Fragment {
    View view;
    ComunicacaoFragmentActivity mCallback;
    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tela_login,null);
        activity = getActivity();
        iniciarBotoesFuncao();
        iniciarBotoesLateral();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (ComunicacaoFragmentActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ComunicacaoFragmentActibity");
        }
    }

    public void iniciarBotoesFuncao (){
        final EditText etNumeroCartao = (EditText)view.findViewById(R.id.et_numero_cartao);
        MainActivity.mCustomKeyboard.registerEditText(etNumeroCartao);

        Button botao = (Button)view.findViewById(R.id.bt_ok_login);
        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ConnLogin(getContext(), etNumeroCartao.getText().toString(), mCallback).execute();
            }
        });
    }

    public void iniciarBotoesLateral(){

        ImageButton botaoEsquerdo1 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_1);
        botaoEsquerdo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoEsquerdo2 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_2);
        botaoEsquerdo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoEsquerdo3 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_3);
        botaoEsquerdo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoEsquerdo4 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_4);
        botaoEsquerdo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });

        ImageButton botaoDireito1 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_1);
        botaoDireito1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoDireito2 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_2);
        botaoDireito2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoDireito3 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_3);
        botaoDireito3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
        ImageButton botaoDireito4 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_4);
        botaoDireito4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
    }

}

class ConnLogin extends AsyncTask<Void, Void, Conta> {

    private String etNumeroCartao;
    private ComunicacaoFragmentActivity mCallback;
    private Context mContext;

    ConnLogin (Context mContext, String etNumeroCartao, ComunicacaoFragmentActivity mCallback){
        this.mContext = mContext;
        this.etNumeroCartao = etNumeroCartao;
        this.mCallback = mCallback;
    }

    @Override
    protected Conta doInBackground(Void... params) {
        //Looper.prepare();
        Conta conta = null;
        try {
            CallHandler callHandler = new CallHandler();
            Client client = new Client(MainActivity.serverIp, 7777, callHandler);
            FrontendService frontendService = (FrontendService) client.getGlobal(FrontendService.class);
            conta = frontendService.buscarContaPeloCartao(etNumeroCartao);
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Conta: " + conta.getId(), "Debug_KElly");
        //Looper.loop();
        return conta;
    }

    protected void onPostExecute(Conta conta) {
        if (conta != null){
            MainActivity.conta = conta;
            mCallback.trocaTela(R.string.tela_home);
            Log.d("Conta chamada: " + conta.getId(), "Debug_kelly");
        }
        else{
            Toast.makeText(mContext, "Número do Cartão Inválido", Toast.LENGTH_LONG);
        }
    }
}