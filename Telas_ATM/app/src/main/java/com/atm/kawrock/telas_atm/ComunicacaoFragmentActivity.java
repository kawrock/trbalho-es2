package com.atm.kawrock.telas_atm;

public interface ComunicacaoFragmentActivity {
    public void trocaTela(int telaASerAberta);
    public void confirmaSenha(int telaASerAberta);
}
