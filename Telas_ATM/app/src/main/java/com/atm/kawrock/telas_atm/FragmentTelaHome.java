package com.atm.kawrock.telas_atm;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

public class FragmentTelaHome extends Fragment {

    ComunicacaoFragmentActivity mCallback;
    View view;
    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tela_home, null);
        activity = getActivity();
        iniciarBotoesFuncao();
        iniciarBotoesLateral();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (ComunicacaoFragmentActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ComunicacaoFragmentActibity");
        }
    }

    public void iniciarBotoesFuncao(){
        Button botaoContaCorrente = (Button) view.findViewById(R.id.bt_conta_corrente);
        botaoContaCorrente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_conta_corrente);
            }
        });
        Button botaoCartaoCredito = (Button) view.findViewById(R.id.bt_cartao_credito);
        botaoCartaoCredito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_cartao_credito);
            }
        });
        Button botaoInvestimentos = (Button) view.findViewById(R.id.bt_investimentos);
        botaoInvestimentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_investimentos);
            }
        });
        Button botaoPagamento = (Button) view.findViewById(R.id.bt_pagamento);
        botaoPagamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_pagamento);
            }
        });
        Button botaoTransferencias = (Button) view.findViewById(R.id.bt_transferencia);
        botaoTransferencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_transferencia);
            }
        });
        Button botaoDepositos = (Button) view.findViewById(R.id.bt_deposito_com_cartao);
        botaoDepositos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_deposito);
            }
        });
    }

    public void iniciarBotoesLateral() {

        ImageButton botaoEsquerdo1 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_1);
        botaoEsquerdo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_conta_corrente);
            }
        });
        ImageButton botaoEsquerdo2 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_2);
        botaoEsquerdo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_cartao_credito);
            }
        });
        ImageButton botaoEsquerdo3 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_3);
        botaoEsquerdo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_investimentos);
            }
        });
        ImageButton botaoEsquerdo4 = (ImageButton) activity.findViewById(R.id.bt_lateral_esquerda_4);
        botaoEsquerdo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });

        ImageButton botaoDireito1 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_1);
        botaoDireito1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_deposito);
            }
        });
        ImageButton botaoDireito2 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_2);
        botaoDireito2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_pagamento);
            }
        });
        ImageButton botaoDireito3 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_3);
        botaoDireito3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.trocaTela(R.string.tela_transferencia);
            }
        });
        ImageButton botaoDireito4 = (ImageButton) activity.findViewById(R.id.bt_lateral_direita_4);
        botaoDireito4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Não faz nada
            }
        });
    }
}
